#!/usr/bin/env bash

set -euo pipefail
SCRIPTDIR="$(pwd)"


log() { echo "$1" >&2; }

TAG="latest"  # Set the tag for your Docker images
REPO_PREFIX="registry.gitlab.com/berry.othmane/microservices-demo66"  # Set your GitLab repository path

while IFS= read -d $'\0' -r dir; do
    # build image
    svcname="$(basename "${dir}")"
    builddir="${dir}"
    #PR 516 moved cartservice build artifacts one level down to src
    if [ "$svcname" == "cartservice" ]; then
        builddir="${dir}/src"
    fi
    image="${REPO_PREFIX}/${svcname}:${TAG}"
    (
        cd "${builddir}"
        log "Building: ${image}"
        docker build -t "${image}" .

        log "Pushing: ${image}"
        docker push "${image}"
    )
done < <(find "${SCRIPTDIR}/src" -mindepth 1 -maxdepth 1 -type d -print0)


log "Successfully built and pushed all images."
